package com.example.app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder


class StringAdapter: RecyclerView.Adapter<StringAdapter.StringHolder>() {
    val stringList = ArrayList<String>()
    class StringHolder(item: View) : RecyclerView.ViewHolder(item){
        var textView: TextView = item.findViewById(R.id.textView)
        fun bind(string: String){
            textView.setText(string)
        }
    }

    override fun getItemCount(): Int {
        return stringList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StringHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.string_item, parent, false)
        return StringHolder(view)
    }

    override fun onBindViewHolder(holder: StringHolder, position: Int) {
        holder.bind(stringList[position])
    }

    fun addString(string:String){
        stringList.add(string)
        notifyDataSetChanged()
    }
}