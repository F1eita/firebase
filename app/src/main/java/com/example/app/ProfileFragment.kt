package com.example.app

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*

class ProfileFragment : Fragment() {

    lateinit var mAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val fragmentLayout = inflater.inflate(R.layout.fragment_profile, container, false)
        val navController = NavHostFragment.findNavController(this)
        initAuth()

        fragmentLayout.btnQuit.setOnClickListener {
            Firebase.auth.signOut()
            navController.navigate(R.id.loginFragment) }

        fragmentLayout.bottom_nav_bar.setupWithNavController(navController)
        fragmentLayout.bottom_nav_bar.setOnNavigationItemSelectedListener { item ->
            navController.navigate(item.itemId)
            return@setOnNavigationItemSelectedListener true
        }

        return fragmentLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val currentUser = mAuth.currentUser
        currentUser?.let{
            for (profile in it.providerData) {
                tvEmail.setText("Email: ${profile.email}")
                tvNickname.setText("Nickname: ${profile.displayName}")
            }
        }
    }
    private fun initAuth(){
        mAuth = Firebase.auth
    }

    companion object {

        @JvmStatic
        fun newInstance() = ProfileFragment()
    }
}