package com.example.app

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.fragment_registration.*
import kotlinx.android.synthetic.main.fragment_registration.view.*


class RegistrationFragment : Fragment() {

    lateinit var mAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentLayout = inflater.inflate(R.layout.fragment_registration, container, false)

        val navController = NavHostFragment.findNavController(this)

        fragmentLayout.btnSignUp.setOnClickListener {
            if (etNickname.getText().toString() == "" || etEmail.getText().toString() == ""
                || etPassword.getText().toString() == "")
                    tvAttention.setText("All fields must be filled")
            else {
                regUser()
            }
        }

        fragmentLayout.login.setOnClickListener { navController.navigate(R.id.loginFragment) }

        initAuth()
        return fragmentLayout
    }

    private fun initAuth(){
        mAuth = Firebase.auth
    }

    private fun regUser(){
        mAuth.createUserWithEmailAndPassword(etEmail.getText().toString(), etPassword.getText().toString())
            .addOnCompleteListener(requireActivity()) { task ->
            if (task.isSuccessful) {

                val user = Firebase.auth.currentUser
                val profileUpdates = userProfileChangeRequest {
                    displayName = etNickname.getText().toString()
                }
                user!!.updateProfile(profileUpdates)

                tvAttention.setText("Registration completed successfully")
            } else {
                tvAttention.setText("Some error")
            }
        }
    }

    companion object {

        @JvmStatic
        fun newInstance() = RegistrationFragment()
    }
}