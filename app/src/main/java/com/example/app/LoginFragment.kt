package com.example.app

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavHostController
import androidx.navigation.fragment.NavHostFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_login.view.*
import kotlinx.android.synthetic.main.fragment_registration.*


class LoginFragment : Fragment() {

    lateinit var mAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentLayout = inflater.inflate(R.layout.fragment_login, container, false)
        val navController = NavHostFragment.findNavController(this)
        initAuth()


        val currentUser = mAuth.currentUser
        if(currentUser != null){
            navController.navigate(R.id.homeFragment)
        }

        fragmentLayout.signIn.setOnClickListener{
            if (fragmentLayout.etUEmail.getText().toString() == ""
                || fragmentLayout.etUPassword.getText().toString() == "")
                tvWrongInput.setText("All fields must be filled")
            else login()
        }
        fragmentLayout.signUp.setOnClickListener { navController.navigate(R.id.registrationFragment) }

        return fragmentLayout
    }

    private fun initAuth(){
        mAuth = Firebase.auth
    }
    private fun login(){
        val navController = NavHostFragment.findNavController(this)
        val email = etUEmail.getText().toString()
        val password = etUPassword.getText().toString()
        mAuth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(requireActivity()) { task ->
                if (task.isSuccessful) {
                    navController.navigate(R.id.homeFragment)
                } else {
                    tvWrongInput.setText("Wrong input!")
                }
            }
    }
    companion object {

        @JvmStatic
        fun newInstance() = LoginFragment()
    }
}