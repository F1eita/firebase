package com.example.app

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*


class HomeFragment : Fragment() {

    val adapter = StringAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentLayout = inflater.inflate(R.layout.fragment_home, container, false)

        val navController = NavHostFragment.findNavController(this)

        fragmentLayout.bottom_nav_bar.setupWithNavController(navController)
        fragmentLayout.bottom_nav_bar.setOnNavigationItemSelectedListener { item ->
            navController.navigate(item.itemId)
            return@setOnNavigationItemSelectedListener true
        }
        // Inflate the layout for this fragment
        return fragmentLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    fun init(){
        rcView.layoutManager = LinearLayoutManager(this.context)
        rcView.adapter = adapter
        for (i in (1..30)){
            adapter.addString("String ${i.toString()}")
        }
    }

    companion object {

        @JvmStatic
        fun newInstance() = HomeFragment()
    }
}